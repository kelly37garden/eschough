package cnu.aoos.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import cnu.aoos.func.FOption;

public interface FStream<T> extends AutoCloseable {

	/**
	 * 스트림에 포함된 다음 데이터를 반환한다.
	 * <p>
	 * 더 이상의 데이터가 없는 경우, 또는 이미 close된 경우에는 {@link FOption#empty()}을 반환함.
	 *
	 * @return 다음 데이터. 없는 경우는 {@link FOption#empty()}.
	 */
	public FOption<T> next();

	public static <T> FStream<T> empty() {
		// (1) implement this method
		return (FStream<T>) StreamSupport.stream(Spliterators.<T> emptySpliterator(), false);

	}

	@SafeVarargs
	public static <T> FStream<T> of(T... values) {
		// (2) implement this method
		return from(Arrays.asList(values).iterator());
	}

	public static <T> FStream<T> from(Iterable<? extends T> values) {
		return (FStream<T>) StreamSupport.stream(Spliterators.spliteratorUnknownSize(values.iterator(), Spliterator.ORDERED), false);
	}

	public static <T> FStream<T> from(Iterator<? extends T> iter) {
		// (3) implement this method
		return (FStream<T>) StreamSupport.stream(Spliterators.spliteratorUnknownSize(iter, 0), false);
	}

	public static <T> FStream<T> from(Stream<? extends T> stream) {
		// (4) implement this method
		return from(stream.iterator());
	}

	/**
	 * 스트림의 첫 count개의 데이터로 구성된 FStream 객체를 생성한다.
	 *
	 * @param count
	 *            데이터 갯수.
	 * @return 'count' 개의 데이터로 구성된 스트림 객체.
	 */
	public default FStream<T> take(long count) {
		// (5) implement this method
		return new FStream<T>() {
			int idx;

			@Override
			public FOption<T> next() {
				if (idx++ < count) {
					FStream.this.next();
				}
				return FOption.empty();
			}

			@Override
			public void close() throws Exception {

			}
		};
	}

	public default FStream<T> drop(long count) {
		// (6) implement this method
		return new FStream<T>() {
			int idx;

			@Override
			public FOption<T> next() {
				if (idx++ > count) {
					return FStream.this.next();
				}
				return FOption.empty();
			}

			@Override
			public void close() throws Exception {

			}
		};
	}

	public default void forEach(Consumer<? super T> effect) {
		// (7) implement this method
		FOption<T> next = FStream.this.next();
		while (next.isPresent()) {
			effect.accept(next.get());
			next = FStream.this.next();
		}
	}

	public default FStream<T> filter(Predicate<? super T> pred) {
		return new FStream<T>() {
			@Override
			public void close() throws Exception {
			}

			// 여기서는 일단 비워둠
			@Override
			public FOption<T> next() {
				FOption<T> next;
				do {
					next = FStream.this.next();
					if (next.isAbsent())
						return FOption.empty();
				} while (!pred.test(next.get()));
				return next;
			}
		};
	}

	public default <S> FStream<S> map(Function<? super T, ? extends S> mapper) {
		// (8) implement this method
		return new FStream<S>() {
			@Override
			public FOption<S> next() {
				FOption<T> next = FStream.this.next();
				if (next.isPresent()) {
					return FOption.of(mapper.apply(next.get()));
				}
				return FOption.empty();
			}

			@Override
			public void close() throws Exception {

			}
		};
	}

	// flatMap: 선택 사항
	// 구현 시 추가 점수 있음
	public default <V> FStream<V> flatMap(Function<? super T, ? extends FStream<V>> mapper) {
		// (9) implement this method (optional)
		return new FStream<V>() {
			@Override
			public FOption<V> next() {
				FOption<T> next = FStream.this.next();
				while (next.isPresent()) {
					FStream<V> apply = mapper.apply(next.get());
					return apply.next();
				}
				return FOption.empty();
			}

			@Override
			public void close() throws Exception {

			}
		};
	}

	public default Iterator<T> iterator() {
		// (10) implement this method
		return new Iterator<T>() {
			@Override
			public boolean hasNext() {
				return FStream.this.next().isPresent();
			}

			@Override
			public T next() {
				return FStream.this.next().get();
			}
		};
	}

	public default ArrayList<T> toList() {
		// (11) implement this method
		return (ArrayList<T>) Arrays.asList(FStream.this);
	}

	public default HashSet<T> toSet() {
		// (12) implement this method
		return new HashSet<T>(FStream.this.toList());
	}

	public default <S> S[] toArray(Class<S> componentType) {
		// (13) implement this method
		return toList().toArray(componentType.getEnumConstants());
	}

	public default Stream<T> stream() {
		// (14) implement this method
		return Stream.of(FStream.this.next().get());
	}

	public default FStream<T> sort(Comparator<? super T> cmp) {
		// (15) implement this method
		return new FStream<T>() {
			@Override
			public FOption<T> next() {
				FOption<T> next = FStream.this.next();
				ArrayList<T> list = new ArrayList<>();
				while (next.isPresent()) {
					list.add(next.get());
				}
				list.sort(cmp);
				return next;
			}

			@Override
			public void close() throws Exception {

			}
		};
	}

	public default boolean isEmpty() {
		return true;
	}

}
